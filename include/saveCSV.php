<?php 
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once("dbPDO.php");

$pdo= DbPDO::getInstance() ; 

function sanitize_input($datax) {
	$sanitizedData = []; 
	foreach($datax as $data )
	{
		$data = trim($data);
  		$data = stripslashes($data);
  		$data = htmlspecialchars($data);
  		$sanitizedData [] =  $data ; 
	} 
  return $sanitizedData;
}

$produits_ids =  sanitize_input($_POST['produits_ids']); 

$produits_prix = sanitize_input($_POST['produits_prix'] );

$produit_quantites = sanitize_input($_POST['produit_quantites'] ); 


$pdo->prepare('INSERT INTO produit (produit_id, prix, quantite)  VALUES(:product_id, :prix, :quantite)') ; 
for($i  = 0 ;  $i < count($produits_ids ) ; $i++ )
{
	$pdo->bindParam(':product_id' ,  $produits_ids[$i] ) ;
	$pdo->bindParam(':prix' ,  $produits_prix[$i] ) ;
	$pdo->bindParam(':quantite' , $produit_quantites[$i] ) ;
	$pdo->execute() ; 
}

$pdo->close();

$_SESSION['saving_success']  = "enregistrement reussi !!" ; 

header("Location: ".$_SERVER['HTTP_REFERER']);
die();