<?php  
class Csv {

	public function  readCSVFile ($fileStream ) {

		$tuples = []; 
		try {
		$file = fopen($fileStream,"r");
		}
		catch(Exception $e) {
 		 	
 		 	   print $e->getMessage()."<br />";
 		}
		while(! feof($file))
		  {
		  	$tuples[]= fgetcsv($file);
		  }

		fclose($file);
		return $tuples  ; 
	}
}