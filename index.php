<?php session_start() ; 

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>CSV</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>
<div class="container">
<?php 
if(!empty($_SESSION['saving_success'] ))
{
	echo  '<div class="alert alert-info" role="alert">'. $_SESSION['saving_success'].'</div>'  ; 
	$_SESSION['saving_success'] = null ;  
}
session_destroy() ; 
	
?>
	<h1>
		Contenu du fichier Csv : csv/data/csvdata.csv
	</h1>
	<form class="form" method="POST" action="./include/saveCSV.php">
	<table class="table table-hover">
		<thead>
			<td>
				id 
			</td>
			<td>
				prix 
			</td>
			<td>
				quantité 
			</td>
		</thead>

		 <?php  
	 	 require_once("include/read.php") ; 
		 foreach ($data as $arTuple) {
		 	?>
		 	<tr name = "">
	  			<td><input  style= "border: none;" name="produits_ids[]" value = "<?php  echo $arTuple[0]  ; ?>" readonly />
	  			</td><td><input  style= "border: none;" name="produits_prix[]" value = "<?php  echo $arTuple[1]  ; ?>" readonly/></td>
	  			<td><input  style= "border: none;" name="produit_quantites[]" value = "<?php  echo $arTuple[2]  ; ?>" readonly/></td>		
	  		</tr>
		 	<?php 
		 }
	 ?>
  		
	
	</table>
	<button type="submit" class="btn btn-primary">save</button>
	</form>
</div>
</body>
</html>